import os
import sys
import random
from datetime import datetime
from twitchio.ext import commands

# set up the bot
bot = commands.Bot(
    irc_token=os.environ['TMI_TOKEN'],
    client_id=os.environ['CLIENT_ID'],
    nick=os.environ['BOT_NICK'],
    prefix=os.environ['BOT_PREFIX'],
    initial_channels=[os.environ['CHANNEL']]
)

# pipenv run python bot.py

film = []
      
film_name =[]
#film_name = os.listdir(path="films/.")
kol_film =len(film)
time_start = datetime.strftime(datetime.now(), "%H:%M:%S")
watch = "/me Не полный список , но потихоньку заполняю. https://docs.google.com/***"
film_words = ["называется фильм","за фильм","!фильм","!фильмы","!Фильм","!Фильмы","фильм?","! фильм"]
moder_name = []

@bot.event
async def event_ready():
    'Called once when the bot goes online.'
    print(f"{os.environ['BOT_NICK']} is online!")
    ws = bot._ws  # this is only needed to send messages within event_ready
    await ws.send_privmsg(os.environ['CHANNEL'], f"/me Bot работает!")


@bot.event
async def event_message(ctx):
    'Runs every time a message is sent in chat.'

    # make sure the bot ignores itself and the streamer
    if ctx.author.name.lower() == os.environ['BOT_NICK'].lower():
        return

    await bot.handle_commands(ctx)

    if ctx.content.lower() in film_words :
        film_long = '/me Сейчас идет -  '
        for i in range(len(film)):
            film_long += str(kol_film-len(film)+i+1) +'.  '+ str(film[i]) +'    '
        await ctx.channel.send(film_long)
        

@bot.command(name='смотрели')
async def test(ctx):
    await ctx.send(watch)

@bot.command(name='след')
async def test(ctx):
    if ctx.author.name in moder_name :
        del(film[0])
        #os.startfile(film_name[0])
        #await ctx.send(str(duration(film_name[0])))
        #film_name.pop(0)
        global time_start
        time_start = '/me Фильм начался в '+ datetime.strftime(datetime.now(), "%H:%M:%S")
        await ctx.send('/me список фильмов изменен')
    else:
        await ctx.send('/me сорян братан')
    
@bot.command(name='следфильм')
async def test(ctx):
    if ctx.author.name in moder_name :
        del(film[0])
        #os.startfile(film_name[0])
        #await ctx.send(str(duration(film_name[0])))
        #film_name.pop(0)
        global time_start
        time_start = '/me Фильм начался в '+ datetime.strftime(datetime.now(), "%H:%M:%S")
        await ctx.send('/me список фильмов изменен')
    else:
        await ctx.send('/me сорян братан')


@bot.command(name='вкл1')
async def test(ctx):
    if ctx.author.name in moder_name :
        os.startfile(film_name[0])
        film_name.pop(0)
        global time_start
        time_start = '/me Фильм начался в '+ datetime.strftime(datetime.now(), "%H:%M:%S")
        await ctx.send('/me список фильмов изменен')
    else:
        await ctx.send('/me сорян братан')

@bot.command(name='добавить')
async def test(ctx):
    if ctx.author.name in moder_name :
        new_film = ["Шоу Трумана","Невероятная жизнь Уолтера Митти","Король говорит!"]
        for i in range(len(new_film)):
            film.insert(4+i,new_film[i])
        global kol_film 
        kol_film = len(film)
        await ctx.send('/me список фильмов изменен')
    else:
        await ctx.send('/me сорян братан')

@bot.command(name='удалить')
async def test(ctx,arg1):
    if ctx.author.name in moder_name :
        film.pop(int(arg1))
        await ctx.send('/me список фильмов изменен')
    else:
        await ctx.send('/me сорян братан')


@bot.command(name='нф')
async def test(ctx,arg):
    if ctx.author.name in moder_name :
        global time_start
        time_start = '/me Фильм начался в '+ arg
        await ctx.send(time_start)     
    else:
        await ctx.send('/me сорян братан , для тебя команда !начало')
        
@bot.command(name='начало')
async def test(ctx):
    await ctx.send(time_start)
    
@bot.command(name='iq')
async def test(ctx):
    iq_long = str(random.randint(1,150))
    iq = '/me Твой iq = '+ iq_long
    await ctx.send(iq)


@bot.command(name='выкл')
async def test(ctx):
    if ctx.author.name in '***' :
        await ctx.send('/me Пока босс')
        sys.exit(0)
    else:
        await ctx.send('/me сорян братан')


@bot.command(name='биба')

async def test(ctx):
    if ctx.author.name in '***' :
        await ctx.send('Самая маленькая в чате, я уже говорил')
    elif ctx.author.name in '***' :
        await ctx.send('/me Самая большая в чате')
    else:
        biba_long = str(random.randint(1,30))
        biba = '/me Твоя биби = '+ biba_long +' см'
        await ctx.send(biba)

if __name__ == "__main__":
    bot.run()
